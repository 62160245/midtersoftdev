
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProductService {


    private static ArrayList<Product> userList = new ArrayList<>();
    private static Product currentUser = null;
    static{
        load();
    }
    // Create
    public static boolean addProduct(Product user) {
        userList.add(user);
        save();
        return true;
    }

    // Delete
    public static boolean delProduct(Product user) {
        userList.remove(user);
        save();
        return true;
    }

    // Delete User
    public static boolean delUser(int index) {
        userList.remove(index);
        save();
        return true;
    }
    public static boolean clearUser() {
        userList.clear();
        save();
        return true;
    }
    // Read

    public static ArrayList<Product> getProduct() {
        save();
        return userList;
    }

    public static Product getUser(int index) {
        save();
        return userList.get(index);
    }
    // Update

    public static boolean updateUser(int index, Product user) {
        userList.set(index, user);
        return true;
    }
    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("Product.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(userList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;

        try {
            file = new File("Product.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            userList = (ArrayList<Product>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
